Creació i validació de documents RSS
====================================

MP4UF2A1EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Exercici pràctic.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en el tractament de
documents RSS i la seva combinació amb fulls d’estil CSS.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 50% de la nota del resultat d’aprenentatge associat
a l’activitat.
