Presentar formularis i dades utilitzant taules
==============================================

MP4UF3A1T2

Simple composició de blocs en pantalla

Taules HTML
-----------

Les taules HTML serveixen per presentar llistats de dades i, si està
justificat, composar informació en pantalla. Aquesta darrera
funcionalitat s’ha d’usar amb precaució, però en el cas dels formularis
sembla raonable fer-la servir.

Cal estudiar, en la documentació de referència del llenguatge HTML,
aquests elements:

-   [table](http://htmlhelp.com/reference/html40/tables/table.html) –
    Taula
    -   [caption](http://htmlhelp.com/reference/html40/tables/caption.html)
        – Títol de la taula
    -   [colgroup](http://htmlhelp.com/reference/html40/tables/colgroup.html)
        – Grup de columnes
        -   [col](http://htmlhelp.com/reference/html40/tables/col.html)
            – Definició d’una columna
    -   [thead](http://htmlhelp.com/reference/html40/tables/thead.html)
        – Capçalera de la taula
    -   [tfoot](http://htmlhelp.com/reference/html40/tables/tfoot.html)
        – Peu de la taula
    -   [tbody](http://htmlhelp.com/reference/html40/tables/tbody.html)
        – Cos de la taula
    -   [tr](http://htmlhelp.com/reference/html40/tables/tr.html) – Una
        fila de la taula
        -   [td](http://htmlhelp.com/reference/html40/tables/td.html) –
            Cel·la de dades
        -   [th](http://htmlhelp.com/reference/html40/tables/th.html) –
            Cel·la de capçalera

**Atenció**: farem servir sempre XHTML estricte i usarem CSS sempre que
sigui possible.

Enllaços recomanats
-------------------

-   [WDG HTML 4.0
    Reference](http://htmlhelp.com/distribution/wdghtml40.tar.gz)
    ([local](../../UF1/A3/aux/wdghtml40.tar.gz))
-   [XHTML Cheat
    Sheet](http://www.cheat-sheets.org/saved-copy/htmlcheatsheet.pdf)
    ([local](../../UF1/A3//aux/htmlcheatsheet.pdf))

Pràctiques
----------

-   Presenta amb ajuda de taules els formularis realitzats en la
    tasca anterior.
-   Presenta en forma tabular els resultat d’una jornada esportiva, com
    ara la darrera jornada de futbol.

